const app = require('express')()

app.get('/', (req, res) => {
    res.send('Hello world from Nodejs!')
})

app.listen(3200, () => {
    console.log('Rimotr is on port 3200....')
})